Section Minimal_propositional_logic.

  Variables P Q R T: Prop.

  Lemma id_P : P->P.
  Proof (fun (p: P) => p).

  Lemma id_P' : P->P.
  Proof.
    intro.
    assumption.
  Qed.

  Lemma id_PP : (P->P)->(P->P).
  Proof fun (p: P->P) => p.
  Print id_PP.

  Lemma id_PP' : (P->P)->(P->P).
  Proof.
    intro.
    assumption.
  Qed.
  Print id_PP'.

  Lemma imp_trans: (P->Q)->(Q->R)->P->R.
  Proof fun (a : P->Q)(b: Q->R)(p : P) => b (a p).
  Print imp_trans.

  Lemma imp_trans': (P->Q)->(Q->R)->P->R.
  Proof.
    intros a b p.
    apply b.
    apply a.
    assumption.
  Qed.
  Print imp_trans'.

  Lemma imp_perm: (P->Q->R)->(Q->P->R).
  Proof fun (f: P->Q->R) => fun (q: Q)(p: P) => f p q.
  Print imp_perm.

  Lemma imp_perm': (P->Q->R)->(Q->P->R).
  Proof.
    intro f.
    intros q p.
    apply f.
    assumption.
    assumption.
  Qed.
  Print imp_perm'.

  Lemma ignore_Q: (P->R)->P->Q->R.
  Proof fun (f: P->R)(p:P)(_:Q) => f p.
  Print ignore_Q.

  Lemma ignore_Q': (P->R)->P->Q->R.
  Proof.
    intros f p q.
    apply f.
    assumption.
  Qed.
  Print ignore_Q'.

  Lemma delta_imp : (P->P->Q)->P->Q.
  Proof fun (f: P->P->Q)(p:P) => f p p.
  Print delta_imp.

  Lemma delta_imp' : (P->P->Q)->P->Q.
  Proof.
    intros f p.
    apply f.
    assumption.
    assumption.
  Qed.
  Print delta_imp'.

  Lemma delta_impR : (P->Q)->(P->P->Q).
  Proof fun(f:P->Q) => fun(p1:P)(p2:P) => f p1.
  Print delta_impR.

  Lemma delta_impR' : (P->Q)->(P->P->Q).
  Proof.
    intros f p1 p2.
    apply f.
    assumption.
  Qed.
  Print delta_impR'.

  Lemma diamond: (P->Q)->(P->R)->(Q->R->T)->P->T.
  Proof fun(f:P->Q)(g:P->R)(d:Q->R->T)(p:P) => d (f p) (g p).
  Print diamond.

  Lemma diamond': (P->Q)->(P->R)->(Q->R->T)->P->T.
  Proof.
    intros f g d p.
    apply d.
    apply f.
    2:apply g.
    assumption.
    assumption.
  Qed.
  Print diamond'.

  Lemma weak_pierce_auto: ((((P->Q)->P)->P)->Q)->Q.
  Proof.
    auto.
  Qed.
  Print weak_pierce_auto.

(*
  Lemma weak_pierce: ((((P->Q)->P)->P)->Q)->Q.
  Proof fun (f: (((P->Q)->P)->P)->Q) => Q.
*)

  Lemma weak_pierce': ((((P->Q)->P)->P)->Q)->Q.
  Proof.
    intro f.
    apply f.
    intro g.
    apply g.
    intro p.
    apply f.
    intro h.
    assumption.
  Qed.
  Print weak_pierce'.

End Minimal_propositional_logic.

Print All.
Print id_P.
Print id_P'.
Print imp_trans.
Print imp_trans'.
Print imp_perm.
Print imp_perm'.
Print ignore_Q.
Print ignore_Q'.
Print delta_imp.
Print delta_imp'.
Print delta_impR.
Print delta_impR'.
Print diamond.
Print diamond'.
Print weak_pierce_auto.
Print weak_pierce'.
