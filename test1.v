
Inductive bool : Set :=
  | true
  | false.

Print bool_ind.
Print bool_rec.
Print bool_rect.

Theorem bool_only : forall b : bool, b = true \/ b = false.
Proof.
  intros.
  destruct b.
  left. reflexivity.
  right. reflexivity.
(*
  constructor 1. reflexivity.
  constructor 2. reflexivity.
*)
Qed.

Check bool_only.
Print bool_only.

Check 1.
Check nat.
Check True \/ False.
Check unit.
Check (1, 2).
Section Foo.
  Variable x : nat.
  Check (x, x).
End Foo.

Parameter P : Prop.
Lemma obvious: P -> P.
Proof.
  intro p.
  assumption.
Qed.

Print obvious.
 
Lemma obvious' : 1 + 1 = 2.
Proof.
  simpl.
  reflexivity.
  Show Proof.
Qed.

Print obvious'.

Compute 1 + 2.
