
Require Import Arith.

Parameters (prime_divisor: nat -> nat)
           (prime : nat -> Prop)
           (divides : nat -> nat -> Prop).

Open Scope nat_scope.

Check prime (prime_divisor 220).
Check divides (prime_divisor 220) 220.
Check divides 3.

Parameter binary_word : nat -> Set.
(*

Definition short : Set := binary_word 32.
Definition long : Set := binary_word 64.

Definition bin32 := binary_word 32.
Definition bin64 := binary_word 64.

Print bin32.
Print bin64.

Section Section_4_1_1_3.
  Section a1.
    Variables (A : Set) (B : Type).
    Check A -> B.
  End a1.
  Section a2.
    Variables (A : Set) (B : Prop).
    Check A -> B.
  End a2.
  Section a3.
    Variables (A : Set) (B : Set).
    Check A -> B.
  End a3.
End Section_4_1_1_3.

Eval cbv in 2 <= 2.

Check (and, or, not).

Check not (divides 3 81).

Check let d := prime_divisor 220 in prime d /\ divides d 220.

Section tuple_or_prod.
  Definition n12 := (1, 2).
  Check (n12).
  Check (nat * nat)%type.
  Check (prod nat nat).
  Definition NN := (nat * nat)%type.
  Check (n12 : nat*nat).
  Definition n23 : nat*nat := (2, 3).
  Definition nn : NN := (1, 2). 
  Definition NN2 := prod nat nat.
  Definition nn2 : NN2 := (1, 2).
  Require Import String.
  Open Scope string_scope.
  Definition typeTuple := (nat, string).
  Check (typeTuple).
  Definition tuple1 := (1, "a").
  Check tuple1.
(*
  Check (tuple1 : tupleT).
*)
  Check (tuple1 : prod nat string).
  Definition tupleT' := prod nat string.
  Check (tuple1 : tupleT').
  Check prod.
  Check fun A B : Type => (A, B).
  Check fun A B : Set => (A, B).
  Check fun A B : Type => prod A B.
  Check fun A B : Set => prod A B.
  Open Scope type_scope.
  Check fun A B : Type => A * B.
  Check fun A B : Set => A * B.
  Check prod Set nat.
  Check (tuple1 : prod nat string).
  Check (prod nat string).
  Print All.
End tuple_or_prod.

Section List_example.
  Check list.
  Check list nat.
  Require Import List.
  Definition Decomp := nat -> list nat.
  Check Decomp.
  Print Decomp.
  Parameters (decomp : nat -> list nat) (decomp2 : nat -> nat * nat).
  Print decomp.
  Print decomp2.
  Check decomp.
  Check decomp2.
  Check (decomp 220).
  Check (decomp2 284).
  Definition dc := (fun (n : nat) => n :: n :: n :: nil) : Decomp.
  Check dc.
  Compute dc 1.
  Compute dc 2.
  Definition dc2 := fun (n : nat) => (n, n + 1).
  Compute dc2 1.
  Compute dc2 2.
End List_example.

Check prod.
Check (list, list).
(* Following gives "Error: Universe inconsistency" *)
(*
Check (prod, list).
*)

Section On_binding.
  Print pair.
  Check pair.
  Print fst.
  Check fst.
  Print snd.
  Check snd.
  Print nil.
  Check nil.
  Print cons.
  Check cons.
  Compute nat :: string :: nil.
(* list Prop isn't compatible with list Set
  Compute nat :: string :: (1 < 2) :: nil.
*)
End On_binding.

Section Examples.
  Check (forall A B : Set, A -> B -> A * B).
  Definition prime_divisor_correct : forall (n : nat), 
    2 <= n -> let d := prime_divisor n in prime d /\ divides d n.
  Abort All.
End Examples.

*)
Section Example_on_ordering.
  Print le_n.
  Check le_n.
  Print le_S.
  Check le_S.
  Check le_n 36.
  Check le_S 11 11 (le_n 11).
  Definition true_11_le_12 := le_S 11 11 (le_n 11).
  Check le_S 11 12 (true_11_le_12).
 
  Definition le_36_37 := le_S 36 36 (le_n 36).
  Check le_36_37.
  Definition le_36_38 := le_S 36 37 (le_36_37).
  Check le_36_38.
  Check (le_S _ _ (le_S _ _ (le_n 36))).
End Example_on_ordering.


Section Program_correctness.
(*
  Check (prime_divisor_correct 220).
*)
End Program_correctness.

(* From p83 4.2.1.3 Polymorphism *)
Section Iterate.
  Variable iterate : forall A : Set, (A -> A) -> nat -> A -> A.

  Check (iterate nat).
  Check (iterate _ (mult 2)).
  Check (iterate _ (mult 2) 10).
  Check (iterate _ (mult 2) 10) 1.
  (* if you have a proper definition then this result = 1024 *)
  Eval compute in (iterate _ (mult 2) 10 1).
  Require Import ZArith.
(*
  Error: The term "36" has type "nat" while it is expected to have type "Z".
  Check (iterate Z (Zmult 2) 5 36).
*)
  Check (iterate Z (Zmult 2) 5 36%Z).
End Iterate.

Section Twice.
  Definition twice : forall A : Set, (A -> A) -> A -> A := fun A f a =>  f (f a).
  Check (twice Z).
  Check (twice Z (fun z => (z*z)%Z)).
  Check (twice _ S 56).
  Check (twice (nat->nat)(fun f x => f (f x))(mult 3)).
  Eval compute in
    (twice (nat->nat)(fun f x => f (f x))(mult 3) 1).
End Twice.

Section Data_types_depending_on_value.
  Variable binary_word_concat : forall (n:nat) (p:nat), 
    binary_word n -> binary_word p -> binary_word (n+p).
  Check (binary_word_concat 32).
  Check (binary_word_concat 32 32).
  Definition binary_word_duplicate (n:nat)(w:binary_word n) : binary_word (n+n)
    := binary_word_concat _ _ w w.
  Check binary_word_duplicate.
End Data_types_depending_on_value.

Section Proof.
  Theorem le_i_SSi : forall i:nat, i <= S(S i).
  Proof fun i:nat => le_S _ _ (le_S _ _ (le_n i)).
End Proof.

Print le_i_SSi.

Section Implicit_arguments.
  Set Implicit Arguments.
  Definition compose (A B C : Set)(f : A -> B)(g : B -> C)(a : A) := g (f a).
  Print compose.
  Definition thrice (A : Set)(f : A -> A) := compose f (compose f f).
  Print thrice.
  Compute thrice (fun (n: nat) => n + 3) 1.
  Eval cbv delta in (thrice (thrice (A := nat)) S 0).
  Eval cbv delta beta in (thrice (thrice (A := nat)) S 0).
  Compute thrice (thrice (A := nat)) S 0.
  Check thrice (thrice (A:=nat)) S 0.
  Compute thrice (thrice S) 0.
  Print S.
End Implicit_arguments.
