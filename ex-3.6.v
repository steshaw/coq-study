
Section Auto6.

  Hypotheses P Q : Prop.

  Lemma break_auto : (((((((((P -> Q) -> Q) -> Q) -> Q) -> Q) -> Q) -> Q) -> Q) -> Q) -> P -> Q.
  Proof.
    auto 6.
  Qed.

End Auto6.
