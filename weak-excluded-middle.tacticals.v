Print not.

Section Weak_excluded_middle.
  Hypothesis P : Prop.

  Lemma weak_excluded_middle: forall P, ~~(P \/ ~P).
  Proof.
    unfold not; intro h1; intro h2; apply h2; right; intro h3; apply h2; left; assumption.
  Qed.
End Weak_excluded_middle.

Print weak_excluded_middle.
