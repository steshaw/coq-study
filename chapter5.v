(* Not about Chapter 5 at all but at our meeting which was supposed to be about chapters 5 and 6 *)

Require Import List.

Check list.

Check (list nat).

Check (list Set).

Check (list (Set -> Set)).

Check nat :: list nat :: nil.

Check 0 :: 1 :: nil.

Check (fun c => list (list c)).

Definition P : Type -> Type := fun c => list c.

Check P :: nil.

Check P :: (fun c => P (P c)) :: nil.

Check nil.

Inductive hlist : list Type -> Type := 
  | hnil : hlist nil
  | hcons : forall {t : Type} {ts : list Type}, t -> hlist ts -> hlist (t :: ts).

Definition hlist1 (ts: list Type): Type :=
  match ts with
    | nil => unit
    | t::ts => (t * hlist ts)%type
  end.

Inductive day := mon | tue.

Check hcons.

Check hcons 42 hnil.

(* Check hlist (nat :: nil). *)
