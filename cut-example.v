Section Minimal_propositional_logic.
  Hypotheses P Q R T : Prop.

  Section cut_example.
    Hypotheses (H: P->Q) (H0: Q->R) (H1: (P->R)->T->Q) (H2: (P->R)->T).
    Theorem cut_example_book : Q.
    Proof.
      cut (P->R).
      intro H3.
      apply H1; [assumption | apply H2; assumption].
      intro H3; apply H0; apply H; assumption.
    Qed.
    Theorem cut_example_long_easy : Q.
    Proof.
      cut (P->R).
      intro H3.
      apply H1.
      assumption.
      apply H2.
      assumption.
      intro H3.
      apply H0.
      apply H.
      assumption.
    Qed.
    Theorem cut_example_not_cut : Q.
    Proof.
      apply H1.
      2:apply H2.
      intro p.
      2:intro p2.
      apply H0.
      apply H.
      assumption.
      apply H0.
      apply H.
      assumption.
    Qed.
    Theorem cut_example_not_cut' : Q.
    Proof.
      apply H1.
      2:apply H2.
      intro p1.
      2:intro p2.
      1:apply H0.
      2:apply H0.
      1:apply H.
      2:apply H.
      1:exact p1.
      exact p2.
    Qed.
    Lemma p2r : P->R.
    Proof.
      intro p.
      apply H0.
      apply H.
      assumption.
    Qed.
    Theorem cut_example_not_cut_with_lemma : Q.
      apply H1.
      exact p2r.
      apply H2.
      exact p2r.
    Qed.
    (* using assert is like a shorthand lemma *)
    Theorem assert_example : Q.
    Proof.
      assert (p2r: (P->R)).
      intro p; apply H0; apply H; assumption.
      apply H1.
      assumption.
      apply H2.
      assumption.
    Qed.

    Print cut_example_long_easy.
    Print cut_example_book.
    Print cut_example_not_cut.
    Print cut_example_not_cut'.
    Print cut_example_not_cut_with_lemma.
    Print assert_example.
      
  End cut_example.

End Minimal_propositional_logic.
