
Section Weak_pierce.

  Hypotheses P Q : Prop.

  Lemma weak_pierce : ((((P -> Q) -> P) -> P) -> Q) -> Q.
  Proof
    fun f : (((P -> Q) -> P) -> P) -> Q =>
      f (fun g : (P -> Q) -> P => 
        g (fun p : P => 
          f (fun _ : (P -> Q) -> P => p))).

End Weak_pierce.
