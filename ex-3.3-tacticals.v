Section Minimal_propositional_logic.

(*
  Tacticals:
    composition tac1; tac2
    generalised-composition: tac; [tac1 | tac2]
    orelse ||
    idtac
    fail
    try
*)

  Variables P Q R T: Prop.

  Lemma id_P : P->P.
  Proof.
    intro; assumption.
  Qed.

  Lemma id_PP : (P->P)->(P->P).
  Proof.
    intro; assumption.
  Qed.
  Print id_PP.

  Lemma imp_trans: (P->Q)->(Q->R)->P->R.
  Proof.
    intros a b p; apply b; apply a; assumption.
  Qed.
  Print imp_trans.

  Lemma imp_perm: (P->Q->R)->(Q->P->R).
  Proof.
    intro f; intros q p; apply f; assumption.
  Qed.
  Print imp_perm.

  Lemma ignore_Q: (P->R)->P->Q->R.
  Proof.
    intros f p q; apply f; assumption.
  Qed.
  Print ignore_Q.

  Lemma delta_imp : (P->P->Q)->P->Q.
  Proof.
    intros f p; apply f; assumption.
  Qed.
  Print delta_imp.

  Lemma delta_impR : (P->Q)->(P->P->Q).
  Proof.
    intros f p1 p2; apply f; assumption.
  Qed.
  Print delta_impR.

  Lemma diamond: (P->Q)->(P->R)->(Q->R->T)->P->T.
  Proof.
    intros f g d p; apply d; [apply f | apply g]; assumption.
  Qed.
  Print diamond.

  Lemma weak_pierce: ((((P->Q)->P)->P)->Q)->Q.
  Proof.
    intro f; apply f; intro g; apply g; intro p; apply f; intro h; assumption.
  Qed.
  Print weak_pierce.

End Minimal_propositional_logic.

Print All.
Print id_P.
Print imp_trans.
Print imp_perm.
Print ignore_Q.
Print delta_imp.
Print delta_impR.
Print diamond.
