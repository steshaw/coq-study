Section trivial_example.
  Variables P Q : Prop.
  Hypothesis (p: P) (p2q: P -> Q).
  
  Lemma p1: P.
  Proof.
    assumption.
  Qed.
  Lemma p2: P.
  Proof.
    trivial.
  Qed.
End trivial_example.
